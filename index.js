const { specification } = require("@aws-cdk/cfnspec");
const { isTaggableResource } = require("@aws-cdk/cfnspec/lib/schema");
const fs = require('fs');

const spec = specification();

const nonTaggableResources = Object.entries(spec.ResourceTypes).filter(([, resource]) =>
  !isTaggableResource(resource)
).map(([resourceName]) => resourceName);

nonTaggableResources.push('AWS::CDK::Metadata', 'AWS::CloudFormation::Init', 'AWS::CloudFormation::Interface', 'AWS::CloudFormation::Designer')

const OUTPUT_FILE = 'non-taggable-resources.json';

console.log(`Found ${nonTaggableResources.length} non-taggable CloudFormation resources. Output to ${OUTPUT_FILE}`);
fs.writeFileSync(OUTPUT_FILE, JSON.stringify(nonTaggableResources), 'utf8');