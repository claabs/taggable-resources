# taggable-resources

Generates a list of non-taggable CloudFormation resources using AWS CDK's [cfnspec](https://github.com/aws/aws-cdk/tree/master/packages/%40aws-cdk/cfnspec).

## Usage

### Generate List

```shell
# This will install the latest CFN Spec, as it is always evolving
npm i @aws-cdk/cfnspec@latest 
# Generate the JSON
npm start
```
